# Recuperação de Informação / Information Retrieval (2019/2020 MIECT/MEI, DETI, UA)

## Text Indexing and Search Engine - Assignment (Recurso)

Dante Marinho - nº mec: 83672

## Estrutura inicial das pastas e ficheiros do programa

./resources/
    ./queries.relevance.txt
    ./queries.txt
    ./snowball_stopwords_EN.txt
./ClassCorpusReader.py
./ClassIndexer.py
./ClassQuery.py
./ClassTonekizer.py
./config.json
./global_variables.py
./index_analytics.py
./init.py
./init_query.py
./README.md

## Ficheiro de configuração

Antes de executar o programa, deverá fazer a parametrização no ficheiro config.json:

```
{
      "input_folder": " C:\\Users\\Dante\\corpus ",     «---» Pasta de input contendo corpus para serem lidos
      "index_folder": "C:\\index",                      «---» Pasta de output onde será persistido o index
      "tokenizer_type": "--simple",                     «---» Tipo do tokenizer: “--complex” ou “--simple”
      "positional_terms": "False",                      «---» Definir se os posting lists são posicionais: “True” ou “False”
      "percent_memory_limit": 95,                       «---» Percentagem máxima de uso da memória (SPIMI)
      "stemming": "True",                               «---» Definir se realiza o stemming aos tokens: “True” ou “False”
      "stopword_filtering": "True",                     «---» Definir se realiza o stopword filtering aos tokens: “True” ou “False”
      "minimum_length_filtering": 3                     «---» Definir o tamanho mínimo para os tokens
      “show_analytics”: “False”                         «---» Exibe uma análise de ocorrência dos termos ao final da indexação
}

``` 

## Instructions

Existem duas opções para correr o programa:

- INDEX :: Executar a parte da indexação para gerar uma pasta contendo todo o index.
- QUERY :: Executar a parte das queries (consultas). Esta depende da existência do index previamente criado.

## Requisitos

- Python versão 3.6.8 e as seguintes dependências
    - Biblioteca PyStemmer (https://pypi.org/project/PyStemmer/)
    - Para a utilização no Windows é necessário ter instalado o Microsoft Visual C++ Build Tools (https://go.microsoft.com/fwlink/?LinkId=691126)
    - Habilitar o PyStemmer na IDE PyCharm: File >> Settings >> Project: <name-project> >> Project Interpreter >> Símbolo "+" (install new package)

## Como correr o programa

(!) Deverá fazer antes a configuração dos parâmetros no ficheiro config.json.

Para correr a indexação:

```
python init.py
```

Para correr as queries:

```
python init_query.py
```

## Link references

### General help links

- http://www.devfuria.com.br/python/receitas-para-manipular-arquivos-de-texto/
- https://panda.ime.usp.br/pensepy/static/pensepy/10-Arquivos/files.html
- http://www.devfuria.com.br/python/manipulando-arquivos-de-texto/
- https://stackoverflow.com/questions/185936/how-to-delete-the-contents-of-a-folder-in-python
- https://stackoverflow.com/questions/986006/how-do-i-pass-a-variable-by-reference
- https://www.youtube.com/watch?v=FsgVwux2gjw (Regular Expressions 2018 | #9 re.compile() for efficiency)
- https://pt.stackoverflow.com/questions/179451/como-achar-e-alterar-uma-linha-especifica-python
- https://www.geeksforgeeks.org/iterate-over-a-list-in-python/
- https://pt.stackoverflow.com/questions/161505/em-python-existe-opera%C3%A7%C3%A3o-tern%C3%A1ria (ternary condition)
- https://www.youtube.com/watch?v=bCVPnnWqY8s&feature=youtu.be
- https://stackoverflow.com/questions/2191699/find-an-element-in-a-list-of-tuples
- https://www.geeksforgeeks.org/python-sort-python-dictionaries-by-key-or-value/
- https://www.geeksforgeeks.org/python-convert-list-of-tuples-to-list-of-strings/
- https://stackoverflow.com/questions/663171/how-do-i-get-a-substring-of-a-string-in-python

### Issues

- https://stackoverflow.com/questions/34399172/why-does-my-python-code-print-the-extra-characters-%C3%AF-when-reading-from-a-tex

### Errors

UnicodeDecodeError: 'utf-8' codec can't decode byte 0xa9 in position 0: invalid start byte
- https://stackoverflow.com/questions/11283220/memory-error-in-python


