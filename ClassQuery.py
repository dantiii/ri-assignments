import datetime
import math
import statistics
import sys

import ClassTokenizer
import global_variables

# =========================================
# = Student :: Dante Marinho | mec: 83672 =
# =========================================

# The IndexDictionary class stores the tokens to feed the index list in memory
class QueryClass:

    def __init__(self):
        # self.letters = []
        self.tokenizer = ClassTokenizer.TokenizerClass()
        self.score = {}

    # Search term line: term:idf;doc_id:w_td;doc_id:w_td;... on index files
    def get_term_index(self, term):
        file_name = term[0] + '.txt'
        # print('Accessing file', file_name)
        ref_file = open(global_variables.index_folder + '/' + file_name, 'r')
        line = ref_file.readline()
        while line:
            if line.startswith(term + ':', 0, len(term) + 1):
                return line
            line = ref_file.readline()

    # Method to calculate the cosine score of the terms
    def cosine_scores(self, query_terms):

        print('\n~ ~ ~ COSINE SCORES ~ ~ ~')

        for term in query_terms:
            
            # Get idf
            term_index = self.get_term_index(term)  # Get the one line from the index file
            if term_index:  # if term exist
                fields_term_index = term_index.split(';')
                idf = fields_term_index[0][len(term + ':'):]

                for i_docs in range(1, len(fields_term_index)):
                    doc_id = fields_term_index[i_docs].split(':')[0]
                    w_td = fields_term_index[i_docs].split(':')[1]
                    if doc_id not in self.score:
                        self.score[doc_id] = float(w_td) * float(idf)
                    else:
                        self.score[doc_id] += float(w_td) * float(idf)

    # This method retuns a documento with Precision, Recall, Avg Precision and nDCG
    def evaluations(self, query_id, results):
        
        file_queries_relevance = open(global_variables.queries_relevance_file, 'r')
        print('Caminho:', global_variables.queries_relevance_file)
        line = file_queries_relevance.readline()
        found = False  # found the ID
        finished = False  # read all same ID
        pmids_query_relevance = []
        relevances = []  # store relevances to calculate the iDCG
        pmids_and_relevances = []  # list of tuples

        while not finished:
            if not found and line.startswith(query_id):
                found = True
            if found and not line.startswith(query_id):
                finished = True
            if line.startswith(query_id):
                fields = line.split('\t')
                pmid = fields[1]
                relevance = fields[2]
                pmids_query_relevance.append(pmid)
                relevances.append(relevance)
                pmids_and_relevances.append(tuple((pmid, relevance)))
            line = file_queries_relevance.readline()
        # print('Lista do ID {}: {}'.format(query_id, pmids_query_relevance))

        # Calculating the Precision and accumulating TP values to after calculate the Average Precision
        pmids_results = []  # create a list with only PMID from PMIDs_query_relevance
        relevances_results = []  # store relevance list with only docs in result query

        # Counting tp and fp
        tp = 0.0  # true positive
        fp = 0.0  # false positive
        sum_tp = 0
        for res in results:
            if res[0] in pmids_query_relevance:
                tp += 1
                sum_tp += res[1]
                relevance = [int(item[1]) for item in pmids_and_relevances if res[0] in item]
                relevances_results.append(relevance[0])
            else:
                fp += 1
            # build the list with only PMIDs_query_relevance
            pmids_results.append(res[0])

        # print('\n - - - - - - - -')
        print('\nPara o ID {} tinham {} tp e {} fp.'.format(query_id, tp, fp))
        # precision = tp / (tp + fp)
        precision = tp / (tp + fp) if tp + fp > 0 else 0  # novo -----------------------------

        # Calculating the Recall
        fn = 0  # false negative
        for pmid in pmids_query_relevance:
            if pmid not in pmids_results:
                fn += 1
        recall = tp / (tp + fn)

        # Calculating the Average Precision
        average_precision = sum_tp / tp if tp > 0 else 0

        # Calculating the DCG
        dcg = 0
        if len(relevances_results) > 0:
            dcg = relevances_results[0]
            for i in range(1, len(relevances_results)):
                log2_i = math.log2(i)
                # print('math.log2(i)', log2_i)
                if log2_i == 0:
                    dcg += 0
                else:
                    dcg += (int(pmids_and_relevances[i][1]) / math.log2(i))

        # print('DCG:', dcg)

        # Calculating the iDCG
        pmids_and_relevances.sort(key=lambda tup: tup[1], reverse=True)
        idcg = int(pmids_and_relevances[0][1])
        for i in range(1, len(pmids_and_relevances)):
            log2_i = math.log2(i)
            # print('math.log2(i)', log2_i)
            if log2_i == 0:
                idcg += 0
            else:
                idcg += (int(pmids_and_relevances[i][1]) / math.log2(i))

        # print('iDCG:', idcg)

        # Calculating nDCG
        ndcg = (round(dcg / idcg, 5)) if idcg > 0 else 0
        # print('nDCG:', ndcg)

        return {'precision': precision, 'recall': recall, "average_precision": average_precision, 'ndcg': ndcg}
            

    # Start point to run the query list        
    def run_query(self):

        # Get values from input arguments
        # global_variables.tokenizer_type = argv  # separate a list into variables
        print('O tipo de tokenizer é', global_variables.tokenizer_type)

        #  Vars to accumulating sum of results
        sum_precision = 0
        sum_recall = 0
        sum_average_precision = 0
        sum_ndcg = 0
        sum_f_measure = 0
        query_counter = 0

        # Vars to get the min values
        min_precision = sys.maxsize
        min_recall = sys.maxsize
        min_average_precision = sys.maxsize
        min_ndcg = sys.maxsize
        min_f_measure = sys.maxsize

        # Vars to get the max values
        max_precision = -sys.maxsize
        max_recall = -sys.maxsize
        max_average_precision = -sys.maxsize
        max_ndcg = -sys.maxsize
        max_f_measure = -sys.maxsize

        # Ranking for n results length (10, 20 and 50)
        number_ranking_results = [10, 20, 50]
        for number_ranking_result in number_ranking_results:
            
            query_throughput = 0
            median_query_latencies = []  # store each latency to after calculate the median
            min_median_query_latency = datetime.timedelta(999999)
            max_median_query_latency = datetime.timedelta(-999999)
            
            query_throughput_init = datetime.datetime.now()

            query_file = open(global_variables.queries_file, 'r')  # Open query file
            line = query_file.readline()
            query_id_means = 0

            while line:

                query_counter += 1  # incrementing one query to query counter

                median_query_latency_init = datetime.datetime.now()

                # split query
                query_id, query_terms = line.split('\t')[0], line.split('\t')[1]
                query_id_means = query_id

                print('\n-----------------------------------------------------------------')
                print('Termo de Pesquisa:', query_terms)
                print('-----------------------------------------------------------------')

                # Tokenizing terms (need to be the same method of the index process
                if global_variables.tokenizer_type == '--simple':  # type is '--simple'
                    query_terms = self.tokenizer.simple_tokenizer(query_terms.lower())
                else:  # type is '--complex'
                    query_terms = self.tokenizer.complex_tokenizer(query_terms.lower())
                # print('Query tokenizada:', query_terms)            
                
                # Calculate the cosine score of the terms
                self.cosine_scores(query_terms)

                median_query_latency_finish = datetime.datetime.now()
                median_query_latency = median_query_latency_finish - median_query_latency_init
                median_query_latencies.append(median_query_latency)

                # Create a list of tuples (doc, score) sorted by score   
                tuples_doc_score_sorted = sorted(self.score.items(), key=lambda x: x[1], reverse=True)

                # Iterate over the sorted sequence :: [:20] show only the first 20 results
                # for elem in tuples_doc_score_sorted[:number_ranking_result]:
                    # print(elem[0] , "::" , elem[1])

                evaluations = self.evaluations(query_id, tuples_doc_score_sorted[:number_ranking_result])
                print('Precision, recall, average precision, nDCG:', evaluations)

                precision = evaluations['precision']
                recall = evaluations['recall']
                average_precision = evaluations['average_precision']
                ndcg = evaluations['ndcg']

                # Calculate F-Measure (harmonic mean)
                f_measure = 0
                if precision > 0 and recall > 0:
                    f_measure = 2 * recall * precision / (recall + precision)

                # Accumulating measures
                sum_precision += precision
                sum_recall += recall
                sum_average_precision += average_precision
                sum_ndcg += ndcg
                sum_f_measure += f_measure

                # Lokking for the min values
                if precision < min_precision:
                    min_precision = precision
                
                if recall < min_recall:
                    min_recall = recall
                
                if average_precision < min_average_precision:
                    min_average_precision = average_precision                

                if f_measure < min_f_measure:
                    min_f_measure = f_measure
                
                if ndcg < min_ndcg:
                    min_ndcg = ndcg

                if median_query_latency < min_median_query_latency:
                    min_median_query_latency = median_query_latency

                # Looking fot the max values
                if precision > max_precision:
                    max_precision = precision
                
                if recall > max_recall:
                    max_recall = recall
                
                if average_precision > max_average_precision:
                    max_average_precision = average_precision
                
                if f_measure > max_f_measure:
                    max_f_measure = f_measure
                
                if ndcg > max_ndcg:
                    max_ndcg = ndcg

                if median_query_latency > max_median_query_latency:
                    max_median_query_latency = median_query_latency

                print('Query: "{}"'.format(query_terms))
                print('Returning {} ranking results.'.format(number_ranking_result))

                print('\nPrecision    Recall    Average Precision    F-Measure    nDCG')
                print('-------------------------------------------------------------')
                num_precision = 4
                print('{:0}{:15}{:10}{:20}{:14}'.format(round(precision, num_precision), round(recall, num_precision), round(average_precision, num_precision), round(f_measure, num_precision), round(ndcg, num_precision)))

                # call next query line
                line = query_file.readline()
            query_file.close()

            # Calculating average
            avg_precision = round(sum_precision / query_counter, 3)
            avg_recall = round(sum_recall / query_counter, 3)
            avg_average_precision = round(sum_average_precision / query_counter, 3)
            avg_f_measure = round(sum_f_measure / query_counter, 3)
            avg_ndcg = round(sum_ndcg / query_counter, 3)

            # Calc median query latency
            mql = statistics.median(median_query_latencies)

            print('\n~ ~ ~ AVERAGE, MIN AND MAX OVER ALL QUERIES ~ ~ ~\n')
            print('K = {}    Precision    Recall    Avg Precision    F-Measure    nDCG    Time '.format(number_ranking_result))
            print('Avg{:12}{:13}{:10}{:17}{:13}    {:15}'.format(avg_precision, avg_recall, avg_average_precision, avg_f_measure, avg_ndcg, str(mql)))
            print('Min{:12}{:13}{:10}{:17}{:13}    {:15}'.format(round(min_precision, 3), round(min_recall, 3), round(min_average_precision, 3), round(min_f_measure, 3), round(min_ndcg, 3), str(min_median_query_latency)))
            print('Max{:12}{:13}{:10}{:17}{:13}    {:15}'.format(round(max_precision, 3), round(max_recall, 3), round(max_average_precision, 3), round(max_f_measure, 3), round(max_ndcg, 3), str(max_median_query_latency)))

            print('\n~ ~ ~ EFFICIENCY METRICS ({} results) ~ ~ ~\n'.format(number_ranking_result))
            print('Mean query latency:', mql)

            query_throughput_finish = datetime.datetime.now()
            query_throughput = query_throughput_finish - query_throughput_init
            query_throughput = query_throughput / number_ranking_result

            print('Query throughput:', query_throughput, '\n')