import ClassQuery

# Method to init the Query Class
def init():
    query = ClassQuery.QueryClass()
    query.run_query()
init()