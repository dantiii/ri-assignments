import re
import global_variables
import Stemmer

class TokenizerClass:

    def __init__(self):
        self.minimum_length_filtering = global_variables.minimum_length_filtering

        # RegEx compiled
        self.less_than_minimum_length_filtering = re.compile(r'\b\w{1,' + re.escape(self.minimum_length_filtering) + r'}\b')  # matches if a word has less than three characters
        self.only_alphabet = re.compile(r'[^A-Za-z]')  # matches whenever there isn't letters from A-Z and a-z
        self.alphanumeric_special = re.compile(r"([A-Za-z]+[\d'\-]+[\w'\-]+)|([0-9]+[A-Za-z'\-]+[\w'\-]+)|([a-zA-Z]{3,})")  # letters with numbers more than 3 chars
        self.stemmer = Stemmer.Stemmer("english")  # Init Stemmer for the English language

    # This method is the simple tokenizer (input: string, output: list)
    def simple_tokenizer(self, title):
        return self.less_than_minimum_length_filtering.sub('', self.only_alphabet.sub(' ', title)).split()  # list with only alphabetic chars from length >= 3

    # This method removes stop words and digits (input: list, output: list)
    def remove_stop_word_and_digits(self, terms):
        # filtered_list = [item for item in terms if item not in global_variables.stopwords]
        filtered_list = []
        for item in terms:
            # Not allow items from stop words and digits
            if item not in global_variables.stopwords:  # and not item.isdigit()
                filtered_list.append(item)
        return filtered_list

    # This tokenizer method uses the stop word remover to exclude some terms and digits
    def complex_tokenizer(self, title):
        # Gets all words that match the RegEx and turns them to a list of tuples,
        # and each tuple has the same number of elements as there are groups in the RegEx (3 elements)
        tokens_list_of_tuples = re.findall(self.alphanumeric_special, title)

        # Remove all empty elements from the tuples
        terms_list = []
        for each_tuple_of_tokens in tokens_list_of_tuples:
            valid_tokens = [token for token in each_tuple_of_tokens if token is not '']  # valid_tokens receives a list of all the tokens in the tuples
            terms_list.extend(valid_tokens)  # this list is update with the contents of valid_tokens

        if global_variables.stemming == 'True':  # Applying Stemmer
            terms_list = self.stemmer.stemWords(terms_list)  # receives all tokens present in terms_list stemmed

        if global_variables.stopword_filtering == 'True':  # Removing Stop Words
            terms_list = self.remove_stop_word_and_digits(terms_list)

        return terms_list

# Links
# https://www.w3schools.com/python/python_regex.asp
# https://stackoverflow.com/questions/12985456/replace-all-non-alphanumeric-characters-in-a-string
# https://stackoverflow.com/questions/24332025/remove-words-of-length-less-than-4-from-string
