# coding=utf-8
import os, datetime, re, bisect, sys
from psutil import virtual_memory
import time
import heapq, os, math
import ClassQuery
import global_variables

# ~ ~ ~ This script is for testing ~ ~ ~

query = ClassQuery.QueryClass()
query.run_query()

# ----------------- termos de pesquisa -----------------

# query = ['cell', 'steroids', 'concentration']
# query = ['centrioles', 'localization', 'specific']  # exemplo que enviei ao professor
query = ['unexpected', 'localization', 'suggests', 'that', 'steroids', 'may', 'affect', 'cell']
# query = ['script', 'case', 'the']
# query = ['role', 'autophagy', 'apoptosis']

# palavra = 'amor'
# linha = 'amor:0.7;10605436:0.11:13;10605437:0.08:2'


# -------------------------------- Resultados da indexação com TI + AB --------------------------------
# '''
#     BM25: 24'29''   

#     Corri o programa com o comado:
#     python init.py "F:\UA-arquivos-grandes\RI\Assignment1\corpus" --simple --non-positional-terms 95

#     Resultado:
#     Hora start ................ 2019-12-18 21:19:55.563682
#     Hora fim ................... 2019-12-18 23:23:07.153350
#     Tempo total ................ 2:03:11.589668
#     Nº docs .................... 4591008
#     Process: ................... 1538.5390625 M
#     Total em disco: 4,13GB
# '''

# Só um arquivo de 4GB
# python init.py "F:\UA-arquivos-grandes\RI\Assignment1\corpus-2" --improved --non-positional-terms 95
# Hora start ................ 2019-12-23 01:38:39.600368
# Hora fim ................... 2019-12-23 02:41:04.434755
# Tempo total ................ 1:02:24.834387
# Nº docs .................... 2295504
# Process: ................... 1111.8671875 MB
# -----------------------------------------------------------------------------------------------------


'''
score = {}
def get_term_index(term):
    file_name = term[0] + '.txt'
    print('Acedendo ao arquivo', file_name)
    ref_file = open(global_variables.output_merged_path + '/' + file_name, 'r')
    line = ref_file.readline()
    while line:
        if line.startswith( term + ':', 0, len(term) + 1):
            return line
        line = ref_file.readline()

print('\n~ ~ ~ COSINE SCORES ~ ~ ~')

def cosine_scores(query_terms):
    for term in query_terms:
        
        # Get idf
        term_index = get_term_index(term)
        if term_index:  # if term exist
            fields_term_index = term_index.split(';')
            idf = fields_term_index[0][len(term + ':'):]

            for i_docs in range(1, len(fields_term_index)):
                doc_id = fields_term_index[i_docs].split(':')[0]
                w_td = fields_term_index[i_docs].split(':')[1]
                if doc_id not in score:
                    score[doc_id] = float(w_td) * float(idf)
                else:
                    score[doc_id] += float(w_td) * float(idf)
cosine_scores(query)

print('\nListagem do score (sorted):\n',)

# Create a list of tuples sorted by index 1 i.e. value field     
listofTuples = sorted(score.items(), key=lambda x: x[1], reverse=True)
 
# Iterate over the sorted sequence :: [:20] show only the first 20 results
for elem in listofTuples[:20] :
    print(elem[0] , "::" , elem[1])
'''


# def average(results):
#     total = len(results)
#     accum = 0
#     for r in results:
#         accum += r[1]
#         print('r:', r)
#     average = accum / total
#     return average

# def min(results):
#     min = sys.maxsize
#     for r in results:
#         if r[1] < min:
#             min = r[1]
#     return min

# def max(results):
#     max = -sys.maxsize
#     for r in results:
#         if r[1] > max:
#             max = r[1]
#     return max

# print('Average:', average(listofTuples[:20]))
# print('Min:', min(listofTuples[:20]))
# print('Max:', max(listofTuples[:20]))

# def make_query_vector(query):
#     vector = []
#     for q in query:
#         count_q = query.count(q)
#         print(count_q, 'elementos')
#         w_tq = 1 + math.log10(count_q)
#         vector.append(w_tq)
#     return vector
# query_vector = make_query_vector(query)

print('\n\nEND')

''' Resultado antes de corrigir o código

query = ['unexpected', 'localization', 'suggests', 'that', 'steroids', 'may', 'affect', 'cell']

10605436 :: 1.797226310773928
11656251 :: 0.653223206491262
10605437 :: 0.6480180048165138
10605438 :: 0.6426863793863113
10605439 :: 0.43264789306722606
'''

# Links
# https://stackoverflow.com/questions/522563/accessing-the-index-in-for-loops
# https://www.youtube.com/watch?v=TjIrEYWlonE (Lecture 17 — The Vector Space Model - Natural Language Processing | Michigan)










#
# lista = ['a', 'b', 'a', 'c', 'd', 'a', 'f']
# print('Lista: ', lista)
#
# conjunto = set(lista)
# print('Set: ', conjunto)
#
# tup = [(1, 'um')]
# print('Tuplo: ', type((1, 'um')))
#
# print('JOIN')
#
# jooo = [('11894897', '1'), ('11894953', '1'), ('9222314', '1'), ('9222327', '1'), ('11064709', '1'), ('12286545', '1'), ('12179849', '1'), ('12345384', '1')]
# print('String da lista de tuples', str(jooo))
#
# list_tuple_str = [':'.join(i) for i in jooo]
# # res = [''.join(i) for i in test_list]
#
#
# print('list_tuple_str', list_tuple_str)
#
# isTup = ('ola',)
# print('isTup:', type(isTup))
# # x = datetime.datetime.now()
# # print('Hora inicio ... ', x)
#
# # arquivo = open('file.txt', 'r')
# # line = arquivo.readline()
# # line = arquivo.readline()
# # # print(line)
# # if 'segunda' in line:
# #     print('O trexo segunda está presente na linha')
# #     # arquivoW = open('file.txt', 'w')
# #     # arquivoW.write('nooooooooova')
# # arquivo.close()
# # # for i in range(1000):
# # #     print(i)
#
# # x = datetime.datetime.now()
# # print('Hora fim ...... ', x)
#
#
# # # def alterar_linha(path, index_linha, nova_linha):
# # #     with open(path,'r') as f:
# # #         # print()
# # #         line = f.readline()
# # #     with open(path,'w') as f:
# # #         while True:
# # #             if not line:
# # #                 break
# # #             # print('Linha index ', line.index('segunda linha'))
#
# # #             if line.index('segunda') == index_linha:
# # #                 print('Encontrou a linha <-----------')
# # #                 f.write(nova_linha+'\n')
# # #             else:
# # #                 line = f.readline()
# # #                 f.write(line.index())
#
# # # alterar_linha('file.txt', 2, 'terceira linha modificada')
#
# # print("# List #")
# # lista = ['754:5', '123:2', '134:1', '254:5']
# # lista.append('334:4')
# # print(lista)
# # lista.sort()
# # print(lista)
#
# # lista = [
# #     {'love' : ['123:1', '425:2', '774:2']},
# #     {'aline' : ['123:1', '425:2', '774:2']},
# #     {'brave' : ['123:1', '425:2', '774:2']}
# # ]
#
# # lista = {
# #     'love' : ['123:1', '425:2', '774:2'],
# #     'aline' : ['123:1', '425:2', '774:2'],
# #     'brave' : ['123:1', '425:2', '774:2']
# # }
#
# # ref = open('file.txt', 'w')
# # for i in sorted (lista):
# #     ref.write(i + ':' + ','.join(lista[i]) + '\n')
#
# # res = lista.get('love')
# # print('\n\n- - - - -\nlista.get(love)', res)
#
# # # lista.popitem()
# # print(lista)
# # # lista.sort()
# # print(lista)
#
#
#
# # print("\n- - - - - - - - - - - - -\n")
#
# # stri = "Ola minha meninha linda! So       queria, linda, dizer   Que és muito linda mesmo! beijao!"
# # print("stri inicial: ........................", stri + '.')
#
# # # res = stri.find('linda')
#
# # # changes non-alpha chars by a space
# # stri = re.sub('[^0-9a-zA-Z]+', ' ', stri).lower().strip()
# # print("stri sem non-alpha: ..................", stri + '.')
#
# # # remove str with less than 3 chars
# # stri = re.sub(r'\b\w{1,2}\b', '', stri).strip()
# # print("stri sem str com menos de 3 chars: ...", stri + '.')
#
# # stri = re.sub(' +', ' ', stri)
# # print("stri sem espaços pelo meio: ..........", stri + '.')
#
#
#
#
# # listinha = []
# # bisect.insort(listinha, 'b')
# # bisect.insort(listinha, 'a')
# # bisect.insort(listinha, 'p')
# # print('listinha:', listinha)
#
# # listaDic = {}
# # # bisect.insort(listaDic, 'b':'beijinho')
# # # bisect.insort(listaDic, 'a':'beijao')
# # print('listaDic:', listaDic)
#
#
# # print('\n-------------------- SET --------------------\n')
#
# # # SET - coleção desordenada e sem índice. Não permite duplicados.
# # # lista = {'Joao', 'Ana', 'Carlos', 'Ana'}
# # lista = ['Paulinho', 'Joao', 'Ana', 'Carlos', 'Ana', 'Joana']
# # print(lista)
#
# # # não podemos aceder aos elementos de um SET pelo seu índice
# # # print(lista[1]) # TypeError: 'set' object is not subscriptable
#
# # # ver se um item existe
# # if 'Joao' in lista:
# #     print('Ok! Joao existe na lista SET.')
#
# # # print do resultado da verificação (True ou False)
# # print('Joa' in lista)      # True
# # print('Daniel' in lista)    # False
#
# # result = [item for item in lista if item.startswith('Joaaa')]
# # print('result:', result)
# # # indice = lista.index(result[0])
# # # print('indice:', indice)
#
#
# # lis = 'aaa,bbb,ccc,ddd,eee'
# # campos = lis.split(',', 1)
# # print('campos:', campos)
#
# # campos = lis.split(',',1)[-1]
# # print('campos:', campos)
#
#
#
#
#
#
#
#
# # '''
# # primeira linha
# # segunda linha
# # terceira linha do arquivo
# # quarta linha
# # quinta linha
#
#
#
#
#
#
# # def alterar_linha(path, index_linha, nova_linha):
# #     with open(path,'r') as f:
# #         texto = f.readlines()
# #     with open(path,'w') as f:
# #         for i in texto:
# #             if texto.index(i) == index_linha:
# #                 f.write(nova_linha+'\n')
# #             else:
# #                 f.write(i)
# # '''
#
# print('\n STOP WORDS \n')
# stopwords = open('snowball_stopwords_EN.txt', 'r').read().split('\n')
# list = ['love', 'manga', 'the', 'major', 'again', 'children', 'any', '123', 'today', 'car']
# filtered_list = []
# for item in list:
#     if item not in stopwords and not item.isdigit():
#         filtered_list.append(item)
#
# print(stopwords)
# print(list)
# print(filtered_list)
#
#
# print('\n ~ ~ ~ Substring:\n\n')
# nome = 'windows/programas/adobe/file.txt'
# print(nome)
# i = nome.rindex("/")
# sub = nome[None:i]
# print(sub)
#
# print('\n ~ ~ ~ psutil:\n\n')
# mem = virtual_memory()
# mem_total = mem.total
# print(mem_total // (1024 * 1024))
# print('Available:', mem.available // (1024 * 1024))
#
# ram_livre_atual = mem.total - mem.available
#
# total_livre = mem.available // (1024*1024)
# print('RAM livre:', mem.available // (1024*1024))
#
# print('Init operation')
# ram_available_on_ini_operation = mem.available // (1024 * 1024)
#
# # time.sleep(10)
# ram_actual = (mem.available * 100 // ram_available_on_ini_operation) // (1024 * 1024)
# print(ram_actual)
# # Open File para leitura
# # ref = open('file.txt', 'r')


'''
# -----------------------------------------
# Merge de vários ficheiros
# -----------------------------------------
print('\n----------------- MERGE ----------------\n')
algarisms = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
             'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
             'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

files = os.listdir('output')
# print(files)

# This cycle reads the corpus files inside the folder passed as argument
# for i, a in enumerate(algarisms):
#     print(a + ' - ' + str(i) )

# def search_tokens(list, str):
#     for i, token in enumerate(list):
#         if str == token.split(':', 1)[0]:
#             return dict(i=token)

num_docs = 4350876  # valor obtido após o processo de indexacao

for alg in algarisms:
    bag_tokens = {}  # store temporarely the posts os a letter
    flag_exists = False
    for file in files:
        ref_file = open('output/' + file, 'r')
        line = ref_file.readline()

        while line:
            # print(line)
            if alg == line[0]:
                flag_exists = True
                # post = search_tokens(bag_tokens, line.split(';', 1)[0])
                post = bag_tokens.get(line.split(';', 1)[0])  # verifica se o post existe já no dic temporario

                # Insere se nao existe
                if not post:
                    # print('token que nao existe')
                    # print(post)
                    token, docs = line.split(';', 1)
                    bag_tokens[token] = [1, docs.strip()]

                    print('apresentando o novo token adicionado')
                    print(token, bag_tokens[token])
                    print()

                # Se já exister, incrementa o num da frequencia de ocorrencia do token e concatena os doc:weight
                else:
                    docs = line.split(';', 1)[1]  # token da linha
                    bag_tokens[token][0] = bag_tokens[token][0] + 1
                    bag_tokens[token][1] = bag_tokens[token][1] + ';' + docs.strip()
                    print(token, bag_tokens[token])
                # bag_tokens.append(line.strip())
            line = ref_file.readline()

    # Na existencia da ocorrencia de uma letra, entao escreve os posts em ficheiro, calculando antes o IDF de cada um
    if flag_exists:
        # print('Printing a letter list:', bag_tokens)
        file_letter = open('output-merged/' + alg + '.txt', 'w+')
        for token, value in sorted(bag_tokens.items()):
            print('value[0],', value[0])
            idf = math.log10(num_docs / value[0])  # value[0] = number od docs where term occurs
            post = token + ':' + str(round(idf, 3)) + ';' + value[1] + '\n'  # value[0] é o nº ocorrencias que virou no idf, vaçue[1] = doc:weight
            file_letter.write(post)
            # print(line)
        flag_exists = False
'''

# f1 = open('output/index_file-2019-11-15-19-15-08-290100.txt', 'r').read()
# f2 = open('output/index_file-2019-11-15-19-16-03-291504.txt', 'r').read()
#
# print('tipo do f1:', type(f1))
# f_final = heapq.merge(f1, f2)
# print(f_final)

# Links
# https://stackoverflow.com/questions/522563/accessing-the-index-in-for-loops