import os
import re
import shutil
import ClassIndexer
import ClassTokenizer
import global_variables

# =========================================
# = Student :: Dante Marinho | mec: 83672 =
# =========================================

# RegEx copilers
compilerID = re.compile('^PMID')
compilerTI = re.compile('^TI')
compilerAB = re.compile('^AB')
compilerPG = re.compile('^PG  - ')
compilerAD = re.compile('^AD  - ')
compiler6Spaces = re.compile('      ')
compilerSO = re.compile('SO')

class CorpusReaderClass:

    def __init__(self):
        self.list_corpus = []  # init a temporary corpus list
        self.piece = {}  # init a temporary dictionary
        self.num_pieces_by_time = 100000  # num pieces to process time by time
        self.count_docs = 0

        # Object instances
        self.tokenizer = ClassTokenizer.TokenizerClass()
        self.indexer = ClassIndexer.IndexerClass()

    def clear_output_folders(self, output_folders):
        for folder in output_folders:
            if os.path.exists(folder):
                for the_file in os.listdir(folder):
                    file_path = os.path.join(folder, the_file)
                    try:
                        if os.path.isfile(file_path):
                            os.unlink(file_path)
                        elif os.path.isdir(file_path):
                            shutil.rmtree(file_path)
                    except Exception as e:
                        print(e)

    def read_corpus(self):

        print('Running program on mode: {} {} {}% (RAM limit)'.format(global_variables.tokenizer_type, global_variables.positional_terms, global_variables.percent_memory_limit))
        print('Input folder:', global_variables.input_folder)

        # Cleaning output folders (output-temp and output-merged)
        self.clear_output_folders([global_variables.output_temp_path, global_variables.index_folder])

        files = os.listdir(global_variables.input_folder)
        print('Files inside the folder to read corpus', files)

        # This cycle reads the corpus files inside the folder passed as argument
        for file in files:
            refCorpusFile = open(global_variables.input_folder + '/' + file, 'r')  # Open a file
            line = refCorpusFile.readline()  # Iterate each line from actual file
            cycles = 1

            # Cycle for a specific corpus file
            while line:
                flagPieceComplete = False
                # declare a new dictionaty item
                if compilerID.match(line) or compilerTI.match(line) or compilerAB.match(line):
                    fields = line.split('- ')
                    # add a field to the dict item
                    if fields[0] == 'PMID':
                        self.piece[fields[0]] = fields[1].rstrip()  # strip() => trim()
                        self.count_docs += 1
                    elif fields[0] == 'TI  ':
                        self.piece['TI+AB'] = fields[1].strip()
                        line = refCorpusFile.readline()  # verify if title has multiple lines
                        while not compilerPG.match(line):
                            self.piece['TI+AB'] += ' ' + line.lstrip()
                            line = refCorpusFile.readline()
                    else:  # when 'AB  '
                        self.piece['TI+AB'] += ' ' + fields[1].strip()
                        line = refCorpusFile.readline()  # verify if title has multiple lines
                        while compiler6Spaces.match(line):

                            self.piece['TI+AB'] += ' ' + line.strip()
                            line = refCorpusFile.readline()

                elif compilerSO.match(line):
                    flagPieceComplete = True
                    # tokenizer the title
                    if global_variables.tokenizer_type == '--complex':
                        self.piece['TI+AB'] = self.tokenizer.complex_tokenizer(self.piece['TI+AB'].lower())
                    else:
                        # for --simple tokenizer
                        self.piece['TI+AB'] = self.tokenizer.simple_tokenizer(self.piece['TI+AB'].lower())

                # Each piece have two items on its dictionary
                # print('len(self.piece)', len(self.piece))
                if flagPieceComplete:
                    self.list_corpus.append(self.piece)
                    self.piece = {}  # reset dict

                # Call the indexer after n pieces added on self.listCorpus
                if len(self.list_corpus) == self.num_pieces_by_time:
                    print('Indexed a demand of {} docs. Cycle {}.'.format(self.num_pieces_by_time, cycles))
                    cycles += 1

                    # Indexer: call de indexer and pass the n pieces to be indexed and writen on index file
                    # self.indexer.indexer(self.listCorpus)  # from assignment 1 (no more used) # xxx <---------------
                    self.indexer.indexer_spimi(self.list_corpus)
                    self.list_corpus = []  # clear listCorpus

                line = refCorpusFile.readline()
            refCorpusFile.close()  # End of reading the corpus

            # Case for remaining items on piece dictionary
            if len(self.piece) > 0:
                self.list_corpus.append(self.piece)

        # Case for remaining items on listCorpus list
        if len(self.list_corpus) > 0:
            # self.indexer.indexer(self.listCorpus)  # from assignment 1 (no more used)  # xxx <----------------------
            self.indexer.indexer_spimi(self.list_corpus)

        # Case for remaining items on index dictinonary
        if len(self.indexer.index_obj.index) > 0:
            self.indexer.save_index_to_file(True)

        return self.count_docs
