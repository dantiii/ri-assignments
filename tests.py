import Stemmer
import re

dic = {'a': 'letra a', 'b': 'letra b', 'c': 'letra c'}
print(dic)

if 'a' in dic:
    print('Letra A está contido no dic.')

# list_tuples = [('a', '123', 'b'), ('c', '425', 'd'), ('e', '456', 'a')]
list_tuples = [('123', 1), ('234', 2), ('223', 2), ('457', 1), ('223', 1)]
# print('list_tuples[0][1]', list_tuples[0][1])
res = [item[1] for item in list_tuples if item[0] == '223']
res2 = [item[1] for item in list_tuples if '223' in item]

print('\nres:', res)
print('res:', res2)

print()

print(list_tuples)
list_tuples.sort(key=lambda tup: tup[0])
print(list_tuples)
list_tuples.sort(key=lambda tup: tup[1], reverse=True)
print(list_tuples)

print()

for i, item in enumerate(list_tuples, 2):
    print('i', i, 'item:', item)

print()

for i in range(2, len(list_tuples)):
    print('i', i, 'list_tuples[i]:', list_tuples[i])

if 'a' in list_tuples:
    print('Encontrou o item na lista de tuples')
else:
    print('Não está na lista de tuples')

import sys, math
max = float(sys.maxsize)
min = float(-sys.maxsize -1)

print('max:', max)
print('min:', min)

max2 = math.inf
min2 = -math.inf

print('max:', max2)
print('min:', min2)


print('\n\nMEDIAN')
import statistics

items = [6, 1, 8, 2, 3, 1]
items = [5,8,1]


m = statistics.median(items)
print('Median', m)

a = 548
b = 45
c = 8

print('Print')
print('{:3d}{:13d}{:8}'.format(a, b, c))


print('\n --------- Complex ---------\n')


alphanumeric_special = re.compile(r"([A-Za-z]+[\d'\-]+[\w'\-]+)|([0-9]+[A-Za-z'\-]+[\w'\-]+)|([a-zA-Z]{3,})")
title = 'During456 1980 the rate of accumulation  between 25 degrees C and 35 degrees C exhibited a Q10 of 1.8.'



# tokens_list = re.findall(alphanumeric_special, title)  # gets all words that match the RegEx
# terms_list = []
# for group in tokens_list:
#     x = [item for item in group if item is not '']  # x receives a list of all the tokens in the tuples
#     terms_list.extend(x)
# print('tokens_list:', terms_list)


tokens_list = re.findall(alphanumeric_special, title)
# Remove all empty elements from the tuples
terms_list = []
for tuple_of_tokens in tokens_list:
    valid_tokens = [token for token in tuple_of_tokens if token is not '']  # valid_tokens receives a list of all the tokens in the tuples
    terms_list.extend(valid_tokens)
print('terms_list :', terms_list)

# Stemmer
stemmer = Stemmer.Stemmer("english")  # gets a stemmer for the English language (!) Disabled
# --------------------------------------------------------------------------------------------
tokens_list = stemmer.stemWords(terms_list)  # receives all tokens present in terms_list stemmed
print('tokens_list:', tokens_list)
terms_list.clear()
for token in tokens_list:
    if len(token) > 2:  # just to be sure, not really confident in the RegEx
        terms_list.append(token)
print('terms_list :', terms_list)


# https://stackoverflow.com/questions/2191699/find-an-element-in-a-list-of-tuples
# https://www.w3schools.com/python/python_tuples.asp