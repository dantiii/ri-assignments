import datetime
import gc
import math
import os
import psutil
import global_variables

# =========================================
# = Student :: Dante Marinho | mec: 83672 =
# =========================================

# The IndexDictionary class stores the tokens to feed the index list in memory
class IndexDictionaryClass:

    def __init__(self):
        self.index = {}

    def insert_to_index(self, tokens, token, doc_id, new_weight):
        token_exists = self.index.get(token)  # Try find the token in the index
        if global_variables.positional_terms == "True":
            tokens_indexes = [str(i) for i, term in enumerate(tokens) if term == token]
            if not token_exists:
                self.index[token] = [(str(doc_id), str(round(new_weight, 2)), ','.join(tokens_indexes))]
            else:
                self.index[token].append((str(doc_id), str(round(new_weight, 2)), ','.join(tokens_indexes)))
        else:
            if not token_exists:
                self.index[token] = [(str(doc_id), str(round(new_weight, 2)))]
            else:
                self.index[token].append((str(doc_id), str(round(new_weight, 2))))


class IndexerClass:

    def __init__(self):
        self.index_obj = IndexDictionaryClass()

    def save_index_to_file(self, clean_index):

        # Verify if path exists
        folders = global_variables.output_temp_path[0:global_variables.output_temp_path.rindex('/')]  # folders until file name
        if not os.path.exists(folders):
            os.makedirs(folders)  # Create path if not exists

        # Open the output file or create if not exists
        file_index = open(global_variables.index_file_name_temp(), 'w+')
        print('\n# # # File name to saving index:', global_variables.index_file_name_temp())
        for term in sorted(self.index_obj.index):  # verificar este sorted :: é necessário?
            # Write in the output file index
            file_index.write(term + ';' + ';'.join([':'.join(term) for term in self.index_obj.index[term]]) + '\n')
        file_index.close()

        # Updating value of major ammount memory used
        memory_used = global_variables.process_general.memory_info().rss / (1024 * 1024)
        if memory_used > global_variables.major_memory_used:
            global_variables.major_memory_used = memory_used

        # Clear actual index variable to cleanning RAM
        if clean_index:
            self.index_obj.index.clear()
            gc.collect()

    # Method to check if there is RAM available
    def is_memory_available(self):
        mem = psutil.virtual_memory()
        ram_percent_actual = mem.percent
        return True if ram_percent_actual < float(global_variables.percent_memory_limit) else False

    # Indexation using SPIMI approach (two ways: positional or non-positional terms in doc)
    def indexer_spimi(self, list_corpus):

        print('\nReached to the indexer_spimi. Index size: {}.\nConsumo da RAM atual: {}%'.format(len(self.index_obj.index), psutil.virtual_memory().percent))

        for piece in list_corpus:
            doc_id = piece['PMID']
            tokens = piece['TI+AB']
            tokens_unic = set(tokens)  # Transform LIST to SET to get unique occurrence of each token
            accumulator_square_sum = 0  # To after norma calculation
            list_token_wt = []
            for token in tokens_unic:
                count_tokens = tokens.count(token)
                wt = 1 + math.log10(count_tokens)  # Calculating weight for this token
                accumulator_square_sum += (wt ** 2)  # Sum the square of this token
                # print('O termo "{}" occoreu {} vezes e o seu wt = {}'.format(token, count_tokens, wt))
                list_token_wt.append(tuple((token, wt)))  # Add to array of tokens weight

            # Calculating the Vector's Norma
            norma = math.sqrt(accumulator_square_sum)
            list_token_wt_dict = dict(list_token_wt)  # Convert list of tuples to  dictionary ("list_wt_dict"  -> "list_token_wt_dict")

            # Normalizing the vector :: Calculating the new weight of each wt dividing by norma
            for token in tokens_unic:
                new_weight = list_token_wt_dict[token] = list_token_wt_dict[token] / norma
                self.index_obj.insert_to_index(tokens, token, doc_id, new_weight)  # Insert to index (positional or non-positional ways)

                # Main action of SPIMI aproach :: Check if RAM is available and then,
                # dump to file the actual posting lists in the dictionary to flush RAM
                if not self.is_memory_available():
                    print('-----> Memory limit: saving partial index to file and cleaning RAM. <-----')
                    self.save_index_to_file(True)
        print('Finished partial insertion on index')

    # This method merges the output files and organize into various files alphabetically
    def merge_output(self, num_docs):
        init_merge = datetime.datetime.now()
        print('\nInit merging and reorganize index files.')
        algarisms = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                     'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                     'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

        files = os.listdir(global_variables.output_temp_path)

        for alg in algarisms:
            bag_tokens = {}  # store temporarely the posts os a letter
            flag_exists = False
            for file in files:
                ref_file = open(global_variables.output_temp_path + file, 'r')
                line = ref_file.readline()

                while line:
                    if alg == line[0]:
                        flag_exists = True
                        post = bag_tokens.get(line.split(';', 1)[0])  # verifica se o post existe já no dic temporario

                        # Insere se nao existe
                        if not post:
                            token, docs = line.split(';', 1)
                            bag_tokens[token] = [1, docs.strip()]

                        # If already exists, increment num frequency of orrurrence of token and concat the doc:weight
                        else:
                            docs = line.split(';', 1)[1]  # token da linha
                            bag_tokens[token][0] = bag_tokens[token][0] + 1
                            bag_tokens[token][1] = bag_tokens[token][1] + ';' + docs.strip()
                    line = ref_file.readline()

            # If letter exists, entao escreve os posts em ficheiro, calculando antes o IDF de cada termo
            if flag_exists:
                # print('Printing a letter list:', bag_tokens)
                if not os.path.exists(global_variables.index_folder):
                    os.makedirs(global_variables.index_folder)  # Create path if not exists

                file_letter = open(global_variables.index_folder + '/' + alg + '.txt', 'w+', encoding='utf-8')
                for token, value in sorted(bag_tokens.items()):
                    idf = math.log10(num_docs / value[0])  # value[0] = number of docs where term occurs
                    post = token + ':' + str(round(idf, 2)) + ';' + value[1] + '\n'  # value[0] é o nº ocorrencias que virou no idf, vaçue[1] = doc:weight
                    file_letter.write(post)
                flag_exists = False

        print('Merge duration: ..........', datetime.datetime.now() - init_merge)
