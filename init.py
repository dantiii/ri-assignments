import datetime
import ClassCorpusReader
import ClassIndexer
import global_variables
import index_analytics

# =========================================
# = Student :: Dante Marinho | mec: 83672 =
# -----------------------------------------
# Developed with Python 3.6.8
# =========================================

ini = datetime.datetime.now()  # Init counting execution time

# --------------------------------------------------------------
# Object Instances
# --------------------------------------------------------------

corpus = ClassCorpusReader.CorpusReaderClass()
indexer = ClassIndexer.IndexerClass()

# --------------------------------------------------------------
# Main program execution
# --------------------------------------------------------------

count_docs = corpus.read_corpus()  # Execute main part of program returning the num of Corpus Reader
indexer.merge_output(count_docs)  # Call the method to merge and organize the output of indexation
corpus.clear_output_folders([global_variables.output_temp_path])  # Clear the output-temp folder

print('\nIndex process finished! See the result indexation on ' + global_variables.index_folder + ' folder.')

end = datetime.datetime.now()
print('\nHora start ................', ini)
print('Hora fim ...................', end)
print('Tempo total ................', end - ini)
print('Nº docs ....................', count_docs)
print('Process: ................... {} MB'.format(global_variables.major_memory_used))

# Destroy objects
del corpus
del indexer

# Show analytics
if global_variables.show_analytics == 'True':
    print('analy:', global_variables.show_analytics)
    print('\n- - - - - - - - Show Analytics - - - - - - - -')
    index_analytics.first_ten_terms_freq_one()
    index_analytics.ten_terms_highest_freq()