import os
import global_variables

# =========================================
# = Student :: Dante Marinho | mec: 83672 =
# =========================================

print('\n- - - - - - - - Show Analytics - - - - - - - -')

def first_ten_terms_freq_one():
    first_ten_frequency_one = []
    if os.path.exists(global_variables.index_folder):
        files = os.listdir(global_variables.index_folder)
        for file in files:
            ref_index_file = open(global_variables.index_folder + '/' + file, 'r')
            line = ref_index_file.readline()
            while line and len(first_ten_frequency_one) < 10:
                fields = line.split(';')

                if len(fields) == 2:
                    term = fields[0].split(':')[0]
                    doc_id = fields[1].split(':')[0]
                    first_ten_frequency_one.append(tuple((term, doc_id)))
                line = ref_index_file.readline()
    print('\n~ ~ ~ First 10 terms with fequency one ~ ~ ~')
    print('\nTerm                       Document_id')
    print('--------------------------------------')
    for i in range(10):
        term = first_ten_frequency_one[i][0]
        doc_id = first_ten_frequency_one[i][1]
        print('{:27}{:10}'.format(term, doc_id))
    # print(*first_ten_frequency_one, sep='\n')


def ten_terms_highest_freq():
    ten_highest_document_frequency = []
    if os.path.exists(global_variables.index_folder):
        files = os.listdir(global_variables.index_folder)
        for file in files:
            ref_index_file = open(global_variables.index_folder + '/' + file, 'r')
            line = ref_index_file.readline()
            while line:
                count_docs = line.count(';')  # ":" for the assignment 1 work
                ten_highest_document_frequency.append(tuple((line.split(':', 1)[0], count_docs)))
                line = ref_index_file.readline()

    print('\n~ ~ ~ First 15 terms with highest document frequency ~ ~ ~')
    # Sort array by second element(num_docs) in reverse order
    ten_highest_document_frequency = sorted(ten_highest_document_frequency, key=lambda x: x[1], reverse=True)
    print('\nTerm                     Number occurrencies')
    print('----------------------------------------------')

    # Show the 15 first terms with highest num docs
    for i in range(15):
        term = ten_highest_document_frequency[i][0]
        num_occurrencies = ten_highest_document_frequency[i][1]
        print('{:19}{:10}'.format(term, num_occurrencies))

# first_ten_terms_freq_one()
# ten_terms_highest_freq()

# Links
# https://pt.stackoverflow.com/questions/161505/em-python-existe-opera%C3%A7%C3%A3o-tern%C3%A1ria
# https://stackoverflow.com/questions/10695139/sort-a-list-of-tuples-by-2nd-item-integer-value
