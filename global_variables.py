import datetime
import json
import psutil
import os

# =========================================
# = Student :: Dante Marinho | mec: 83672 =
# =========================================

# --------------------------------------------------------------
# Global variables
# --------------------------------------------------------------

process_general = psutil.Process(os.getpid())  # Measuring process :: Memory used
major_memory_used = 0

# Possible configurations and examples to do in config.json file:
# {
#   "input_folder": <"corpus_folder"> | <"corpus"> | <"F:\UA-arquivos-grandes\RI\Assignment1\corpus">, => the folder to read the Corpus files
#   "tokenizer_type": <"--simple"> | <"--complex">,
#   "positional_terms": <"True"> | <"False">,
#   "percent_memory_limit": [80-95], => Percent of memory to trigger the program dump posting list to file
#   "stemming": <"True"> | <"False">,
#   "stopword_filtering": <"True"> <"False">,
#   "minimum_length_filtering": [2-8],
#   "show_analytics": <"True"> <"False">
# }

# Accesing config file to get parameters
with open('config.json', 'r') as config:
    param = json.load(config)

# -------- MAIN VARIABLES (Geting from config.json) --------

input_folder = param['input_folder']
tokenizer_type = param['tokenizer_type']
positional_terms = param['positional_terms']
percent_memory_limit = param['percent_memory_limit']
stemming = param['stemming']
stopword_filtering = param['stopword_filtering']
minimum_length_filtering = str(param['minimum_length_filtering'] - 1)
index_folder = param['index_folder']  # Output merged folder :: The result of indexation process
show_analytics = param['show_analytics']

# -------- OTHER VARIABLES --------

output_temp_path = 'output-temp/'  # Output temp folder

# File to store the output index
def index_file_name_temp():
    return output_temp_path + 'index_file-' + datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S-%f') + '.txt'
    # return './index_file.txt'  # this way specifies to save the output index file in root folder
    # Date format: year-month-day-hour-minut-second
    # date_now = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')

resources_folder = 'resources/'  # Resources base directory for the next files
stopwords = open(resources_folder + 'snowball_stopwords_EN.txt', 'r').read().split('\n')  # Stop words :: read file and split all lines(terms)
queries_file = resources_folder + 'queries.txt'  # Query file
queries_relevance_file = resources_folder + 'queries.relevance.txt'  # Query Relevance file
